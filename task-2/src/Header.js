import React, { Component } from 'react';


class Header extends Component{
    render() {
        return (
        <header>
            <div className="container clearfix">
                <a className="logo" href="#">FilmNet</a>
                <nav className="main-nav">
                    <a href="">Фильмы</a>
                    <a href="">Сериалы</a>
                    <a href="">мультфильмы</a>
                    <a href="">Аниме</a>
                    <a href="">Новинки</a>
                    <a href="">Подборки</a>
                </nav>
            </div>
        </header>
        )
    }
}

export default Header;