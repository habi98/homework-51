import React from "react";

const Card = (props) =>{
    return(
        <div className="card">
            <img src={props.img} className="wrap-img" alt=""/>
            <h3>{props.name}</h3>
            <p>Год выпуска:{props.issue}</p>
        </div>
    );
};


export default Card
