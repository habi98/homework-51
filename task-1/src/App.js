import React, { Component } from 'react';
import  Card from './Card';
import Poster from './iron-men.jpg'
import Poster2 from './diver.jpg'
import Poster3 from './aven.jpeg'

class App extends Component {
  render() {
    return (
        <React.Fragment>
          <Card  img={Poster} name=" Железный человек"   issue=" 2008"/>
            <Card img={Poster2}  name="Дивергент" issue=" 2014"  />
            <Card img={Poster3}    name=" Мстители война бесконечности" issue=" 2018"/>
          </React.Fragment>
    );
  }
}

export default App;
