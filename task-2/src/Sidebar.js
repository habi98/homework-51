import React from "react";

const Sidebar = (props) => {
    return (
      <div className="sidebar">
          <ul className="nav-link">
              <li>Навигация</li>
              <li><a href="#">Новинки Фильмов 2018</a></li>
              <li><a href="#">Фильмы 2017</a></li>
              <li><a href="#">Сериалы</a></li>
              <li><a href="#">Трейлеры</a></li>
              <li><a href="#">Мультфильмы</a></li>
              <li><a href="#">МультСериалы</a></li>
              <li><a href="#">Аниме</a></li>
              <li><a href="#">Телепередачи</a></li>
              <li><a href="#">Топ 100 Фильмов</a></li>
              <li><a href="#">Скоро на Сайте</a></li>
          </ul>

      </div>
    )
}

export default Sidebar